import React,{Component} from 'react';
import { Modal, ModalHeader, ModalBody, Button, Label, Col,Row } from 'reactstrap';
import { Control, LocalForm, Errors } from 'react-redux-form';
const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => val && (val.length >= len);
const isNumber = (val) => !isNaN(Number(val));
const validEmail = (val) => /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(val);



class CommentForm extends Component {
	constructor(props){
	super(props);
	this.state ={
		isModalOpen:false
	};
	this.toggleModal = this.toggleModal.bind(this);
	this.handleLogin = this.handleLogin.bind(this);
	}
	

	toggleModal(){
		this.setState({
			isModalOpen:!this.state.isModalOpen
		});
	}
    handleLogin(){
    	this.toggleModal();

    }


	render(){
		return(
			<>
				<LocalForm >

		          <Button onClick={this.toggleModal} className="fa fa-pencil" type="submit" >
		                Submit Comment
		          </Button>
		        </LocalForm>

		        <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
						<ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
						<ModalBody>
							<LocalForm onSubmit={this.handleLogin}>
								<Row className="form-group">
									 <Col md={12}>
		                                   
		                                        <Label check>
		                                            
		                                                Rating
		                                        </Label>
		                                  
		                                </Col>
		                                <Col md={12}>
		                                    <Control.select model=".contactType" name="contactType"
		                                        className="form-control">
		                                        <option>1</option>
		                                        <option>2</option>
		                                        <option>3</option>
		                                        <option>4</option>
		                                        <option>5</option>
		                                    </Control.select>
		                                </Col>
								</Row>
								<Row className="form-group">
									 <Label htmlFor="firstname" md={12}>Your Name</Label>
		                                <Col md={12}>
		                                    <Control.text model=".yorname" id="yorname" name="yorname"
		                                        placeholder="Your Name"
		                                        className="form-control"
		                                        validators={{
                                            		required, minLength: minLength(3), maxLength: maxLength(15)
                                        		}}
		                                       />
		                                    <Errors
		                                        className="text-danger"
		                                        model=".yorname"
		                                        show="touched"
		                                        messages={{
		                                            required: 'Required',
		                                            minLength: 'Must be greater than 2 characters',
		                                            maxLength: 'Must be 15 characters or less'
		                                        }}
		                                     />
		                                </Col>
								</Row>
								<Row className="form-group">
									 <Label htmlFor="message" md={12}>Comment</Label>
		                             <Col md={12}>
		                                    <Control.textarea model=".message" id="message" name="message"
		                                        rows="12"
		                                        className="form-control" />
		                             </Col>
								</Row>
								<Row className="form-group">
                                   <Col>
									<Button type="submit" value="submit" color="primary">Submit</Button>
								   </Col>

								</Row>

							</LocalForm>
						</ModalBody>	
				</Modal>
		       </> );
	}
		
}

export  default CommentForm;